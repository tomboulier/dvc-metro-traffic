import pandas as pd
from sklearn.metrics import explained_variance_score
from sklearn.metrics import r2_score
from sklearn.metrics import mean_squared_error
from math import sqrt
import logging
from config import OUTPUT_EVALUATE, OUTPUT_TRAIN, OUTPUT_SPLIT


def evaluate(model, X, y) -> pd.DataFrame:
    """Evaluate the model on the test set and return some metrics"""
    y_pred = model.predict(X)
    #metrics = pd.DataFrame.from_dict(classification_report(y, y_pred, output_dict=True)).T
    evs = explained_variance_score(y, y_pred)
    r2 = r2_score(y, y_pred)
    rmse = sqrt(mean_squared_error(y, y_pred))

    metrics = pd.Series({
        "rmse": rmse,
        "explained_variance_score": evs,
        "r2_score": r2
    })
    return metrics


if __name__ == "__main__":
    # Pulling the model
    model = pd.read_pickle(OUTPUT_TRAIN)
    # Pulling the data
    splits = pd.read_pickle(OUTPUT_SPLIT)
    X, y = splits["X_test"], splits["y_test"]
    print(X.shape)
    print(y.shape)
    # Evaluate
    metrics = evaluate(model, X, y)

    # Storing the metrics for DVC
    metrics.to_json(OUTPUT_EVALUATE)
    logging.info("Model has been evaluate with metrics : %d and stored at %s", ", ".join(metrics.index), OUTPUT_EVALUATE)

