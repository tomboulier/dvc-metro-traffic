# Imports
import logging
import pandas as pd
from config import OUTPUT_PREPARE, OUTPUT_FEATURES


def build_features(data_df: pd.DataFrame) -> pd.DataFrame:
    data_df = data_df.drop(["weather_description"], axis=1)
    data_df = pd.get_dummies(data_df)
    return data_df


if __name__ == "__main__":
    input_dict = pd.read_pickle(OUTPUT_PREPARE)  # Pulling data

    output_dict = build_features(input_dict)
    '''
    for df_name in ["X_train", "X_test"]:
        split_data = input_dict[df_name]
        split_data_featured = build_features(split_data)
        output_dict[df_name] = split_data_featured
    '''
    pd.to_pickle(output_dict, OUTPUT_FEATURES)

    logging.info("The dataset has been featured and stored at %s", OUTPUT_FEATURES)
