from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="DVC Example",
    description="Some example of DVC usage in ML project",
    author="JEBO - TONI",
    version="0.0.1",
    author_email="",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=find_packages(),
    package_data={'dvc-example': ['data/*.csv']},
    install_requires=[
        'pandas',
        'scikit-learn',
        'numpy',
        'dvc==0.52.1'
    ],
    extras_require={
    },
    classifiers=[
        "Programming Language :: Python :: 3"
    ]
)
